# CHANGELOG

## 0.2.0 2018/08/27
* Correção de erros de carregamento dos arquivos.

## 0.1.0 2018/08/26
* Versão inicial da biblioteca.
