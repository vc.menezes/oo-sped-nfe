<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota;

use NFePHP\NFe\Make;
use \stdClass;

/**
 * Classe abstrata que fará a chamada dos métodos de construção das tags da nota.
 */
abstract class Makeable
{
    /**
     * Nome do elemento xml da nota que representa um grupo de informações.
     * @var string
     */
    private $tagName;
    
    /**
     * @param string $tagName Nome do elemento xml da nota que representa um grupo de informações.
     * @return void
     */
    protected function __construct(string $tagName)
    {
        $this->tagName = $tagName;
    }

    /**
     * Método que irá popular uma `stdClass` com os itens do elemento da nota.
     * Cada proriedade da classe retornada é um elemento filho.
     * @return stdClass
     */
    abstract protected function buildFields(): stdClass;

    /**
     * Método que fará a construção do elemento.
     * @param Make $make Instância da classe que está construindo o xml.
     * @return void
     */
    final public function make(Make &$make): void
    {
        $methodName = 'tag' . $this->tagName;
        $fields = $this->buildFields();

        $make->$methodName($fields);
    }
}
