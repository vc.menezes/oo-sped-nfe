<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Pagamento;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados de Pagamento. Obrigatório apenas para (NFC-e) NT 2012/004.
 */
final class Pagamento extends Makeable
{
    public function __construct()
    {
        parent::__construct('pag');
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->vTroco = null;

        return $s;
    }
}
