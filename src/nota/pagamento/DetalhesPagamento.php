<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Pagamento;

use OO_NFePHP\Interfaces\IDetalhesPagamento;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Grupo de detalhamento da forma de pagamento.
 */
final class DetalhesPagamento extends Makeable
{
    /**
     * Tipo de Integração do processo de pagamento com o sistema de automação da empresa:
     * - 1=Pagamento integrado com o sistema de automação da empresa Ex. equipamento TEF, Comercio Eletronico;
     * - 2=Pagamento não integrado com o sistema de automação da empresa Ex: equipamento POS;
     */
    private const TIPO_INTEGRACAO = '1';

    /**
     * Dados dos detalhes de pagamento.
     * @var IDetalhesPagamento
     */
    private $detalhes;

    /**
     * @param IDetalhesPagamento $detalhes Dados dos detalhes de pagamento.
     */
    public function __construct(IDetalhesPagamento $detalhes)
    {
        parent::__construct('detPag');
        $this->detalhes = $detalhes;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->indPag = $this->detalhes->getCondicaoPagamento();
        $s->tPag = $this->detalhes->getFormaPagamento();
        $s->vPag = $this->detalhes->getValorPagamento();

        if ($s->tPag === '03') {
            $s->tpIntegra = self::TIPO_INTEGRACAO;
            $CNPJ = $this->detalhes->getCNPJCartao();
            if (!empty($CNPJ)) {
                $s->CNPJ = $CNPJ;
            }
            $tBand = $this->detalhes->getBandeiraCartao();
            if (!empty($tBand)) {
                $s->tBand = $tBand;
            }
            $cAut = $this->detalhes->getCodigoAutorizacao();
            if (!empty($cAut)) {
                $s->cAut = $cAut;
            }
        }

        return $s;
    }
}
