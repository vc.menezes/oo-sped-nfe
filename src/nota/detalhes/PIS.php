<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Detalhes;

use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados do PIS.
 */
final class PIS extends Makeable
{
    /**
     * Código de Situação Tributária do PIS.
     * - 04 - Operação Tributável - Tributação Monofásica - (Alíquota Zero);
     * - 06 - Operação Tributável - Alíquota Zero;
     * - 07 - Operação Isenta da contribuição;
     * - 08 - Operação Sem Incidência da contribuição;
     * - 09 - Operação com suspensão da contribuição;'
     */
    public const CST = '04';

    /**
     * Índice do item da NF-e.
     * @var string
     */
    private $item;
    
    /**
     * @param string $item Índice do item da NF-e.
     */
    public function __construct(string $item)
    {
        parent::__construct('PIS');
        $this->item = $item;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->item = $this->item;
        $s->CST = self::CST;
        // $s->vBC = null;
        // $s->pPIS = null;
        // $s->vPIS = null;
        // $s->qBCProd = null;
        // $s->vAliqProd = null;

        return $s;
    }
}
