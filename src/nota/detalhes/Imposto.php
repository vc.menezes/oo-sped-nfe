<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Detalhes;

use OO_NFePHP\Interfaces\IImposto;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Tributos incidentes nos produtos ou serviços da NF-e.
 */
final class Imposto extends Makeable
{
    /**
     * Índice do item da NF-e.
     * @var string
     */
    private $item;

    /**
     * Classe que contém os dados da nota.
     */
    private $imposto;

    /**
     * @param string $item Índice do item da NF-e.
     * @param IImposto $imposto Dados que contém os dados da nota.
     */
    public function __construct(string $item, IImposto $imposto)
    {
        parent::__construct('imposto');
        $this->item = $item;
        $this->imposto = $imposto;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->item = $this->item;
        $s->vTotTrib = $this->imposto->getValorTotalTributos();

        return $s;
    }
}
