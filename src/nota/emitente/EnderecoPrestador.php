<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Emitente;

use OO_NFePHP\Interfaces\IEndereco;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Tipo Dados do Endereço do Emitente.
 */
final class EnderecoPrestador extends Makeable
{
    /**
     * Dados do endereço.
     * @var IEndereco
     */
    private $endereco;

    /**
     * @param IEndereco $endereco Dados do endereço do prestador.
     */
    public function __construct(IEndereco $endereco)
    {
        parent::__construct('enderEmit');
        $this->endereco = $endereco;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->xLgr = $this->endereco->getLogradouro();
        $s->nro = $this->endereco->getNumeroLogradouro();
        $xCpl = $this->endereco->getComplementoEndereco();
        if (!empty($xCpl)) {
            $s->xCpl = $xCpl;
        }
        $s->xBairro = $this->endereco->getBairro();
        $s->cMun = $this->endereco->getCodigoCidade();
        $s->xMun = $this->endereco->getNomeCidade();
        $s->UF = $this->endereco->getSiglaUF();
        $s->CEP = $this->endereco->getCEP();
        $cPais = $this->endereco->getCodigoPais();
        if (!empty($cPais)) {
            $s->cPais = $cPais;
        }
        $xPais = $this->endereco->getNomePais();
        if (!empty($xPais)) {
            $s->xPais = $xPais;
        }
        $fone = $this->endereco->getNumeroTelefonePrincipal();
        if (!empty($fone)) {
            $s->fone = $fone;
        }

        return $s;
    }
}
