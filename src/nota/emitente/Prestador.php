<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Emitente;

use OO_NFePHP\Interfaces\IPrestador;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Identificação do emitente.
 */
final class Prestador extends Makeable
{
    /**
     * Dados da Identificação do emitente.
     *
     * @var IPrestador
     */
    private $prestador;

    /**
     * @param IPrestador $prestador Dados do prestador.
     */
    public function __construct(IPrestador $prestador)
    {
        parent::__construct('emit');
        $this->prestador = $prestador;
    }

    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->CNPJ = $this->prestador->getCNPJ();
        $s->xNome = $this->prestador->getNomePrestador();
        $s->IE = $this->prestador->getInscricaoEstadual();
        $s->IM = $this->prestador->getInscricaoMunicipal();
        $s->CNAE = $this->prestador->getCNAE();
        $s->CRT = $this->prestador->getCodigoRegimeTributario();

        return $s;
    }
}
