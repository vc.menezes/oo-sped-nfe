<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Transporte;

use OO_NFePHP\Interfaces\IVolume;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados dos volumes.
 */
final class Volume extends Makeable
{
    /**
     * Dados dos volumes.
     */
    private $volume;

    public function __construct(IVolume $volume)
    {
        parent::__construct('vol');
        $this->volume = $volume;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->qVol = $this->volume->getQuantidadeVolume();
        $s->nVol = $this->volume->getNumeracaoVolume();
        $s->pesoB = $this->volume->getPesoBruto();
        $s->pesoL = $this->volume->getPesoLiquido();

        return $s;
    }
}
