<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Transporte;

use OO_NFePHP\Interfaces\ITransportadora;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Dados do transportador.
 */
final class Transportadora extends Makeable
{
    /**
     * Dados da transportadora.
     * @var ITransportadora
     */
    private $transp;

    /**
     * @param ITransportadora $transp Dados da transportadora.
     */
    public function __construct(ITransportadora $transp)
    {
        parent::__construct('transporta');
        $this->transp = $transp;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->CNPJ = $this->transp->getCNPJ();
        $s->xNome = $this->transp->getNome();
        $s->IE = $this->transp->getInscricaoEstadual();
        $s->xEnder = $this->montarEndereco();
        $s->xMun = $this->transp->getNomeCidade();
        $s->UF = $this->transp->getSiglaUF();

        return $s;
    }

    private function montarEndereco(): string
    {
        if (empty($this->transp->getLogradouro())) {
            return '';
        }

        return $this->transp->getLogradouro() . ', ' . $this->transp->getNumeroLogradouro();
    }
}
