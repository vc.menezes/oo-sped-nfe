<?php
declare(strict_types=1);

namespace OO_NFePHP\Nota\Informacoes;

use OO_NFePHP\Interfaces\IInformacoesContribuinte;
use OO_NFePHP\Nota\Makeable;
use \stdClass;

/**
 * Campo de uso livre do contribuinte.
 * Informar o nome do campo no atributo xCampo, e o conteúdo do campo no xTexto.
 */
final class InformacoesContribuinte extends Makeable
{
    /**
     * Classe com as informações a serem inseridas.
     * @var IInformacoesContribuinte
     */
    private $informacoes;

    /**
     * @param IInformacoesContribuinte $informacoes Dados com as informações ao contribuinte.
     */
    public function __construct(IInformacoesContribuinte $informacoes)
    {
        parent::__construct('obsCont');
        $this->informacoes = $informacoes;
    }
    
    protected function buildFields(): stdClass
    {
        $s = new stdClass();
        $s->xCampo = $this->informacoes->getNomeCampo();
        $s->xTexto = $this->informacoes->getTexto();

        return $s;
    }
}
