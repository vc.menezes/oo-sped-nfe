<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Detalhes do pagamento dos produtos da nota.
 */
interface IDetalhesPagamento
{
    /**
     * Indicador da Forma de Pagamento:
     * - 0-Pagamento à Vista;
     * - 1-Pagamento à Prazo;
     * @return string
     */
    public function getCondicaoPagamento(): string;

    /**
     * Forma de Pagamento:
     * - 01-Dinheiro;
     * - 02-Cheque;
     * - 03-Cartão de Crédito;
     * - 04-Cartão de Débito;
     * - 05-Crédito Loja;
     * - 10-Vale Alimentação;
     * - 11-Vale Refeição;
     * - 12-Vale Presente;
     * - 13-Vale Combustível;
     * - 14-Duplicata Mercantil;
     * - 15-Boleto Bancario;
     * - 90-Sem Pagamento;
     * - 99-Outros.
     * @return string
     */
    public function getFormaPagamento(): string;

    /**
     * Valor do Pagamento.
     * Esta tag poderá ser omitida quando a tag tPag for igual 90 (Sem Pagamento).
     * Caso contrário, deverá ser preenchida.
     * @return string
     */
    public function getValorPagamento(): string;

    /**
     * CNPJ da credenciadora de cartão de crédito/débito.
     * @return string
     */
    public function getCNPJCartao(): string;

    /**
     * Bandeira da operadora de cartão de crédito/débito:
     * - 01–Visa;
     * - 02–Mastercard;
     * - 03–American Express;
     * - 04–Sorocred;
     * - 05-Diners Club;
     * - 06-Elo;
     * - 07-Hipercard;
     * - 08-Aura;
     * - 09-Cabal;
     * - 99–Outros.
     * @return string
     */
    public function getBandeiraCartao(): string;

    /**
     * Número de autorização da operação cartão de crédito/débito.
     * @return string
     */
    public function getCodigoAutorizacao(): string;
}
