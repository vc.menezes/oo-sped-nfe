<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Informações de e-mail.
 */
interface IEmail
{
    /**
     * Informar o e-mail do destinatário. O campo pode ser utilizado para informar o e-mail
     * de recepção da NF-e indicada pelo destinatário.
     * @return string
     */
    public function getEmailPrincipal(): string;
}
