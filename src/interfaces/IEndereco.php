<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados do Endereço do destinatário ou emitente da nota fiscal.
 */
interface IEndereco extends IMunicipio, IEstado, IPais, ITelefone
{
    /**
     * Logradouro. Nome da rua, avenida, travessa, etc.
     * @return string
     */
    public function getLogradouro(): string;

    /**
     * Número do logradouro.
     * @return string
     */
    public function getNumeroLogradouro(): string;

    /**
     * Complemento do endereço.
     * @return string
     */
    public function getComplementoEndereco(): string;

    /**
     * Bairro do endereço.
     * @return string
     */
    public function getBairro(): string;

    /**
     * CEP - NT 2011/004.
     * @return string
     */
    public function getCEP(): string;
}
