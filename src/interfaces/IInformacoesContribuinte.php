<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Campo de uso livre do contribuinte.
 */
interface IInformacoesContribuinte
{
    /**
     * Nome do campo para referência.
     * @return string
     */
    public function getNomeCampo(): string;

    /**
     * Texto com a informação que se deseja inserir na nota.
     * @return string
     */
    public function getTexto(): string;
}
