<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados necessários para as informações sobre o estado.
 */
interface IEstado
{
    /**
     * Código da UF. Utilizar a Tabela do IBGE.
     * @return string
     */
    public function getCodigoUF(): string;

    /**
     * Sigla da UF.
     * @return string
     */
    public function getSiglaUF(): string;
}
