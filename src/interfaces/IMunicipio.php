<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados necessários para as informações sobre o município.
 */
interface IMunicipio
{
    /**
     * Código do municipio. Utilizar a Tabela do IBGE.
     * @return string
     */
    public function getCodigoCidade(): string;
  
    /**
     * Nome do município.
     * @return string
     */
    public function getNomeCidade(): string;
}
