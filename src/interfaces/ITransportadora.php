<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados da transportadora do produto.
 */
interface ITransportadora extends IMunicipio, IEstado
{
    /**
     * CNPJ da transportadora.
     * @return string
     */
    public function getCNPJ(): string;
  
    /**
     * Nome fantasia da tranportadora.
     * @return string
     */
    public function getNome(): string;

    /**
     * Número da inscrição estadual da transportadora.
     * @return string
     */
    public function getInscricaoEstadual(): string;

    /**
     * Nome da rua, avenida, travessa, etc.
     * @return string
     */
    public function getLogradouro(): string;

    /**
     * Número do endereço.
     * @return string
     */
    public function getNumeroLogradouro(): string;
}
