<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados do Telefone.
 */
interface ITelefone
{
    /**
     * Preencher com Código DDD + número do telefone (v.2.0).
     * @return string
     */
    public function getNumeroTelefonePrincipal(): string;
}
