<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Para representar os dados necessários para as informações de país.
 */
interface IPais
{
    /**
     * Código do País.
     * @return string
     */
    public function getCodigoPais(): string;
  
    /**
     * Nome do País.
     * @return string
     */
    public function getNomePais(): string;
}
