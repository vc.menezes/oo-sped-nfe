<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Encapsulamento das mensagens retornadas aos usuários.
 * Questões de internacionalização devem ser tratadas aqui.
 */
interface IMensagem
{
    /**
     * Texto para mensagem:
     * Caminho do certificado inválido, ou o arquivo não está acessível.
     * @return string
     */
    public function caminhoDoCertificadoInvalidoOuArquivoInexistente(): string;

    /**
     * Texto para mensagem:
     * É necessário compilar o XML antes de tentar assiná-lo.
     * @return string
     */
    public function ehNecessarioCompilarOXmlAntesDeAssinalo(): string;

    /**
     * Texto para mensagem:
     * Erro na leitura do certificado digital. Mensagem: %s.
     * @return string
     */
    public function erroNaLeituraDoCertificadoDigital(string $errorMessage): string;

    /**
     * Texto para a mensagem:
     * Não foi possível assinar o arquivo XML. Mensagem: $s.
     * @return string
     */
    public function naoFoiPossivelAssinarOArquivoXML(string $errorMessage): string;
}
