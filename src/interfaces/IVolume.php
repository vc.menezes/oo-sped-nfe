<?php
declare(strict_types=1);

namespace OO_NFePHP\Interfaces;

/**
 * Dados dos volumes.
 */
interface IVolume
{
    /**
     * Quantidade de volumes transportados.
     * @return string
     */
    public function getQuantidadeVolume(): string;

    /**
     * Numeração dos volumes transportados.
     *
     * maxLength value="60"
     * @return string
     */
    public function getNumeracaoVolume(): string;

    /**
     * Peso bruto do volume transportado.
     * @return string
     */
    public function getPesoBruto(): string;

    /**
     * Peso líquido do volume transportado.
     * @return string
     */
    public function getPesoLiquido(): string;
}
