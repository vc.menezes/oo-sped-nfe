<?php
declare(strict_types=1);

namespace OO_NFePHP\Signer;

use OO_NFePHP\Signer\ValidationItem;

/**
 * Validates if the path is valid.
 */
final class CaminhoInvalido extends ValidationItem
{
    /**
     * The path to the pfx file.
     * @var string
     */
    private $certificatePath;

    /**
     * @param string $certificatePath The path to the certificate file.
     */
    public function __construct(string $certificatePath)
    {
        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $item, string &$mensagem): bool
    {
        if (empty($this->certificatePath) || !file_exists($this->certificatePath)) {
            $mensagem = $this->msg->caminhoDoCertificadoInvalidoOuArquivoInexistente();
            return false;
        }
        return true;
    }
}
