<?php
declare(strict_types=1);

namespace OO_NFePHP\Signer;

use OO_NFePHP\Signer\ValidationItem;

/**
 * Checks if the file is empty.
 */
final class ArquivoVazio extends ValidationItem
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    public function validate(string $item, string &$mensagem): bool
    {
        if (empty($item)) {
            $mensagem = $this->msg->ehNecessarioCompilarOXmlAntesDeAssinalo();
            return false;
        };
        return true;
    }
}
