<?php
declare(strict_types=1);

namespace OO_NFePHP\Signer;

use OO_NFePHP\Container;
use OO_NFePHP\Interfaces\IMensagem;

/**
 * This type is to be used when validating the possibility to sign a xml content.
 */
abstract class ValidationItem
{
    /**
     * @var IMensagem
     */
    protected $msg;

    public function __construct()
    {
        $this->msg = Container::instance()->get(IMensagem::class);
    }

    /**
     * Validates the context.
     * @param string $item The item to be validated.
     * @param string $mensagem the error message to be returned.
     * @return bool
     */
    abstract public function validate(string $item, string &$mensagem): bool;
}
