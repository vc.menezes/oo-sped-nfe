<?php

//Registering all the classes and interfaces via bootstrap.
include_once(__DIR__ . '/bootstrap.php');

use OO_NFePHP\AutoLoader;

//Aditional classes used in tests.
AutoLoader::registerDirectory(__DIR__ . '/tests/mocks');
AutoLoader::registerDirectory(__DIR__ . '/tests/factories');

use OO_NFePHP\Container;

//Load the definitions map for the dependency injection.
Container::instance(__DIR__ . '/tests/mapping/definitionsMap.php');
