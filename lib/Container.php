<?php
declare(strict_types=1);

namespace OO_NFePHP;

use DI\ContainerBuilder;

/**
 * The depencendy injection class.
 */
class Container
{
    /**
     * @var Psr\Container\ContainerInterface|DI\FactoryInterface
     */
    private $container;

    /**
     * Static instance of the class
     * @var self
     */
    private static $instancia = null;

    /**
     * @param string $definitionsPath Path to the class mapping file.
     */
    private function __construct(string $definitionsPath = '')
    {
        $builder = new ContainerBuilder();

        if (!empty($definitionsPath)) {
            $builder->addDefinitions($definitionsPath);
        }

        $this->container = $builder->build();
    }

    /**
     * Gets the single instance, static style.
     * @param string $definitionsPath Path to the class mapping file.
     */
    public static function instance(string $definitionsPath = ''): self
    {
        if (is_null(self::$instancia)) {
            self::$instancia = new self($definitionsPath);
        }

        return self::$instancia;
    }

    /**
     * Get an instance of the class.
     * @param string $className The name of the class to get.
     * @param array $args Supply the constructor of the desired class the parameters that cannot be guessed via IoC.
     * @return object
     */
    public function get(string $className, array $args = []): object
    {
        if (empty($args)) {
            return $this->container->get($className);
        }

        return $this->container->make($className, $args);
    }

    /**
     * Check if the class is mapped.
     * @param string $className The name of the class to search for.
     * @return bool
     */
    public function has(string $className): bool
    {
        return $this->container->has($className);
    }

    public function call(string $class, string $method, array $params): object
    {
        return $this->container->call([$class, $method], $params);
    }
}
