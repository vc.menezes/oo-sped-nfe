<?php

namespace OO_NFePHP;

use \DirectoryIterator;

/**
 * Loads all the classes and interfaces to the resources memory.
 */
final class AutoLoader
{
    /**
     * The name of all the classes in the mapped directories.
     * @var string[]
     */
    private static $classNames = array();

    /**
     * Store the filename (sans extension) & full path of all ".php" files found.
     * @param string $dirName The path that will be searched for all the .php files.
     * @return void
     */
    public static function registerDirectory(string $dirName): void
    {
        $di = new DirectoryIterator($dirName);

        foreach ($di as $file) {
            if ($file->isDir() && !$file->isLink() && !$file->isDot()) {
                // recurse into directories other than a few special ones
                self::registerDirectory($file->getPathname());
            } elseif (substr($file->getFilename(), -4) === '.php') {
                $classFullName = self::getClassFullName($file->getFilename(), $file->getPathname());
                self::registerClass($classFullName, $file->getPathname());
            }
        }
    }
    
    /**
     * Register a sincle class to the mapped resources list.
     * @param string $className The name of the class to be registered.
     * @param string $fileName Full path to the class file.
     * @return void
     */
    private static function registerClass(string $className, string $fileName): void
    {
        self::$classNames[$className] = $fileName;
    }

    /**
     * Gets the full name of the class, witch include it's namespace.
     * @param string $fileName The name of the file.
     * @param string $filePath The full path of the file.
     * @return string The namespace of the class, plus it's name.
     */
    private static function getClassFullName(string $fileName, string $filePath): string
    {
        $content = file_get_contents($filePath);
        preg_match('/namespace\s.+;/', $content, $matches);
        $namespace = substr($matches[0], 10, strlen($matches[0])-11);
        $className = substr($fileName, 0, -4);

        return $namespace . '\\' . $className;
    }
    
    /**
     * Load a sincle class or interface to the resources memory.
     * @param string $className The name of the class to be loaded.
     * @return void
     */
    public static function loadClass(string $className): void
    {
        if (isset(self::$classNames[$className])) {
            require_once(self::$classNames[$className]);
        }
    }
}
