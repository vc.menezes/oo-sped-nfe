<?php
declare(strict_types=1);

namespace OO_NFePHPTest;

use OO_NFePHP\Builder;
use OO_NFePHP\Container;

use OO_NFePHP\Interfaces\IProduto;
use OO_NFePHP\Interfaces\IImposto;
use OO_NFePHP\Interfaces\IInformacoesContribuinte;

/**
 * Factory para instanciação da classe `Builder`.
 */
final class BuilderFactory
{
    /**
     * Uma instância limpa da classe, passados parâmetros quaisquer.
     * @param bool $pj Informe verdadeiro para usar a pessoa jurídica.
     * @return Builder
     */
    public static function instanciaNova(bool $pj = false): Builder
    {
        /**
         * The dependency injector.
         */
        $di = Container::instance();

        return $di->get(Builder::class, array(
            'ambiente' => 2,
            'destinatario' => $di->get($pj ? 'DestinatarioPJ' : 'DestinatarioPF'),
            'detalhes' => array(
                array($di->get(IProduto::class), $di->get(IImposto::class)),
            ),
            'informacoesAoContribuinte' => array(
                $di->get(IInformacoesContribuinte::class)
            ),
        ));
    }
}
