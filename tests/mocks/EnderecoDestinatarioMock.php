<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IEndereco;

final class EnderecoDestinatarioMock implements IEndereco
{
    public function getLogradouro(): string
    {
        return 'Rua do Destinatario Teste';
    }

    public function getNumeroLogradouro(): string
    {
        return '100';
    }

    public function getComplementoEndereco(): string
    {
        return 'Apto 666';
    }

    public function getBairro(): string
    {
        return 'Parque Testado I';
    }

    public function getCEP(): string
    {
        return '86600000';
    }
    
    public function getCodigoCidade(): string
    {
        return '1100031';
    }
  
    public function getNomeCidade(): string
    {
        return 'CABIXI';
    }
    
    public function getCodigoUF(): string
    {
        return '11';
    }

    public function getSiglaUF(): string
    {
        return 'RO';
    }
    
    public function getCodigoPais(): string
    {
        return '1058';
    }
  
    public function getNomePais(): string
    {
        return 'Brasil Varonil';
    }
    
    public function getNumeroTelefonePrincipal(): string
    {
        return '55999456660';
    }
}
