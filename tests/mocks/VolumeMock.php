<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IVolume;

final class VolumeMock implements IVolume
{
    public function getQuantidadeVolume(): string
    {
        return '1';
    }

    public function getNumeracaoVolume(): string
    {
        return '0';
    }

    public function getPesoBruto(): string
    {
        return '1';
    }

    public function getPesoLiquido(): string
    {
        return '1';
    }
}
