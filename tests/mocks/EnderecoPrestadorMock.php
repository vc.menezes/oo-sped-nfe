<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IEndereco;

final class EnderecoPrestadorMock implements IEndereco
{
    public function getLogradouro(): string
    {
        return 'Rua do Prestador Teste';
    }

    public function getNumeroLogradouro(): string
    {
        return '777';
    }

    public function getComplementoEndereco(): string
    {
        return 'Apto 2';
    }

    public function getBairro(): string
    {
        return 'Jardim da Unidade';
    }

    public function getCEP(): string
    {
        return '87700000';
    }
    
    public function getCodigoCidade(): string
    {
        return '1100023';
    }
  
    public function getNomeCidade(): string
    {
        return 'ARIQUEMES';
    }
    
    public function getCodigoUF(): string
    {
        return '11';
    }

    public function getSiglaUF(): string
    {
        return 'RO';
    }
    
    public function getCodigoPais(): string
    {
        return '1058';
    }
  
    public function getNomePais(): string
    {
        return 'Brasil Varonil';
    }
    
    public function getNumeroTelefonePrincipal(): string
    {
        return '98999456660';
    }
}
