<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IProduto;

final class ProdutoMock implements IProduto
{
    public function getCodigoProduto(): string
    {
         return 'CFOP9999';
    }

    public function getCodioEAN(): string
    {
         return '';
    }

    public function getDescricaoProduto(): string
    {
         return 'Teste de Unidade palável';
    }

    public function getCodigoNCM(): string
    {
         return '11223344';
    }

    public function getCEST(): string
    {
         return '100';
    }

    public function getCFOP(): string
    {
         return '12345';
    }

    public function getUnidadeComercial(): string
    {
         return 'UN';
    }

    public function getQuantidadeComercial(): string
    {
         return '1';
    }

    public function getValorUniarioComercializacao(): string
    {
         return '49.90';
    }

    public function getValorBrutoProduto(): string
    {
         return '49.90';
    }

    public function getEANUnidadeTributavel(): string
    {
         return '';
    }

    public function getUnidadeTributavel(): string
    {
         return '1';
    }

    public function getQuantidadeTributavel(): string
    {
         return '1';
    }

    public function getValorUnitarioTributacao(): string
    {
         return '49.90';
    }

    public function getValorFrete(): string
    {
         return '50.00';
    }

    public function getValorDesconto(): string
    {
         return '0.01';
    }

    public function getIndicadorValorTotal(): string
    {
         return '1';
    }

    public function getPedidoDeCompra(): string
    {
         return '123000';
    }

    public function getNumeroItemPedidoDeCompra(): string
    {
        return '123';
    }
}
