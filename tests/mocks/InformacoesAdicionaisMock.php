<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Mocks;

use OO_NFePHP\Interfaces\IInformacoesAdicionais;

final class InformacoesAdicionaisMock implements IInformacoesAdicionais
{
    public function getInformacoesAoFisco(): string
    {
        return 'Fisco: Este é um teste de unidade, sem valor fiscal.';
    }

    public function getInformacoesComplementares(): string
    {
        return 'Informações complementares de teste.';
    }
}
