<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Builder;

use \TypeError;
use PHPUnit\Framework\TestCase;
use OO_NFePHP\Builder;
use OO_NFePHPTest\BuilderFactory;

final class ConstructTest extends TestCase
{
    public function testDeveFalharSeChamarConstrutorSemArgumentoAlgum(): void
    {
        $this->expectException(TypeError::class);
        $b = new Builder();
    }

    public function testDeveInstanciarClasseQuandoPassadoArgumentosCorretamente(): void
    {
        $builder = BuilderFactory::instanciaNova();
        $this->assertInstanceOf(Builder::class, $builder);
    }
}
