<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Builder;

use PHPUnit\Framework\TestCase;
use OO_NFePHPTest\BuilderFactory;

final class SignTest extends TestCase
{
    private static $file;
    private static $fileName;

    public static function setUpBeforeClass()
    {
        self::$fileName = __DIR__ . '/ExistentFile.pfx';
        self::$file = fopen(self::$fileName, "w");
        fclose(self::$file);
    }

    public static function tearDownAfterClass()
    {
        unlink(self::$fileName);
    }

    public function testDeveRetornarFalsoQuandoTentandoAssinarAntesDeCompilarArquivo(): void
    {
        $builder = BuilderFactory::instanciaNova();
        $certificado = self::$fileName;
        $senha = '12345';
        $result = $builder->sign($certificado, $senha);

        $this->assertFalse($result);
    }

    public function testDeveRetornarFalsoQuandoParametroCertificadoForVazio(): void
    {
        $builder = BuilderFactory::instanciaNova();
        $certificado = '';
        $senha = '';
    
        $builder->build();
        $result = $builder->sign($certificado, $senha);

        $this->assertFalse($result);
    }
  
    public function testDeveRetornarFalsoQuandoPassadoCaminhoParaUmCertificadoQueNaoExiste(): void
    {
        $builder = BuilderFactory::instanciaNova();
        $certificado = __DIR__ . '/NonExistentFile.pfx';
        $senha = '12345';
    
        $builder->build();
        $result = $builder->sign($certificado, $senha);

        $this->assertFalse($result);
    }
}
