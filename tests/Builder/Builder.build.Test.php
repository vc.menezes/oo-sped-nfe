<?php
declare(strict_types=1);

namespace OO_NFePHPTest\Builder;

use PHPUnit\Framework\TestCase;
use OO_NFePHPTest\BuilderFactory;

final class BuildTest extends TestCase
{
    public function testDeveRetornarVerdadeiroAposChamadaDoMetodoQuandoMontadoArquivoComSucesso(): void
    {
        $builder = BuilderFactory::instanciaNova();
        $result = $builder->build();

        $this->assertTrue($result);
    }
    public function testDeveRetornarVerdadeiroAposChamadaDoMetodoQuandoMontadoArquivoComSucesso2(): void
    {
        $builder = BuilderFactory::instanciaNova(true);
        $result = $builder->build();

        $this->assertTrue($result);
    }
}
