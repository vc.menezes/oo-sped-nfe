# Contribuindo
Qualquer pessoa pode contribuir com o projeto, e serão devidamente creditadas aos seus autores.

## Ambiente
Os scripts e comandos utilizados e mencionados ao longo do repositório foram feitos e testados no ambiente Linux Ubuntu [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

## Padrão
Foi adotado o padrão [PSR-2](https://www.php-fig.org/psr/psr-2/), e as ferramentas de _code sniffer_ já configuradas repositório quando instalado as dependências via `composer`.

## Pull Requests
* Sempre confira se seu código não está quebrando o padrão adotado;
* Sempre escreva testes para os códigos que estiver mandando;
* Qualquer envio que faça alteração de comportamento, o arquivo `README.md` e o Wiki do repositório devem ser devidamente atualizados com o novo comportamento.

## Testes
É necessário ter as dependências do projeto já instaladas (via `composer install`), para que o [phpunit](https://phpunit.de/announcements/phpunit-7.html) esteja instalado. Com isso, estando na raiz do repositório, basta executar:
```
$ ./vendor/bin/phpunit
```
para executar TODOS os testes. Ou:
```
$ ./vendor/bin/phpunit --testdox
```
para executar também TODOS os testes, porém mostrando o resultado no padrão [TestDox](https://phpunit.readthedocs.io/en/7.3/textui.html#testdox).

# Boas práticas
Antes de enviar suas alterações, verifique se o código estrá atendendo ao padrão, executando o código:
```
$ ./vendor/bin/phpcs .
```
E para fazer a correção automática de boa parte dos erros no padrão de código, use:
```
$ ./vendor/bin/phpcbf .
```
